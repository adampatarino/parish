<?php

require_once( 'config/bones.php' ); // if you remove this, bones will break
include_once( 'config/Mobile_Detect.php');
// require_once( 'library/admin.php' ); 
// require_once( 'config/custom-post-type.php' ); 
// require_once( 'library/translation/translation.php' );

/**************** MOBILE DETECTION ****************/

// Let's use call certain content for different browsers
$detect = new Mobile_Detect;



/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes



/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __( 'Sidebar 1', 'bonestheme' ),
		'description' => __( 'The first (primary) sidebar.', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));
} 


/************* Print Fuctions ********************/

function to_slug($string){
  return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
}

/************* Enqueue JS Plugins ****************/

add_action('wp_head', 'js_plugins_and_styles', 1);
function js_plugins_and_styles() {
	if (!is_admin()) {
		wp_register_script('magnific', get_stylesheet_directory_uri() . '/library/js/libs/magnific-popup.min.js', array(), '1.0.0');
		wp_register_style('magnific-style', get_stylesheet_directory_uri() . '/library/css/magnific-popup.min.css', array(), '1.0.0');
		
		wp_enqueue_style('magnific-style');
		wp_enqueue_script('magnific');
	} 
}

?>
