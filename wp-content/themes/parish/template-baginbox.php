<?php
/* Template Name: Bag-in-Box */
get_header(); ?>

      <div id="content" class="bib parish-tempate">

        <div id="inner-content" class="clearfix">

            <div id="main" class="clearfix" role="main">

              <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

              <article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

                <header class="page-header">
                 
                  <div class="wrap">
                    <div class="header-image"><img src="<?php the_field('sub_header_image'); ?>"></div> 
                    <div class="title-wrap">
                      <div class="header_title"> <h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1> </div>
                      <div class="breadcrumbs"><?php if(function_exists('bcn_display')) { bcn_display(); }?></div>
                    </div>
                  </div>

                 <!-- Sub Header Stuff Here Zack -->

                </header> <!-- end article header -->
                 
                
                <section class="sub-head wrap">
                 <?php $sub_head = get_field('bib_sub_header'); ?>
                	 <div class="threecol">
                	    <img src="<?php echo $sub_head[0]['image']; ?>">
                    </div>

                	  <div class="ninecol last"> 
                	     <h3><?php echo $sub_head[0]['content']; ?></h3>
                    </div>
                </section>
                
                
                <section class="the-columns wrap cleafix">
                
	                 <div class="sixcol"><?php the_field('content_column_1'); ?></div>
	                 <div class="sixcol last"><?php the_field('content_column_2'); ?></div>
	                
                </section>


                <section class="page-banner">
                  <?php $banner = get_field('page_banner'); ?>
                  <div class="inner-page-banner wrap clearfix">
                    <div class="banner-image fourcol">
                      <img src="<?php echo $banner[0]['image']; ?>"/>
                    </div>
                    <div class="banner-content eightcol last">
                      <h3><?php echo $banner[0]['title']; ?></h3>
                      <div><?php echo $banner[0]['content']; ?></div>
                    </div>

                  </div>
                </section>
                
              
                <footer class="page-contact" style="background-image: url('<?php echo $contact_bg; ?>');">
                  <div class="inner-page-contact wrap clearfix">
                    <?php $contact = get_field('contact_section'); ?>
                          
                    <div class="contact-content-wrap sevencol">
                      <div class="contact-content">
                        <?php echo $contact[0]['content']; ?>
                      </div>
                      <div class="page-contact-form">
                        <?php echo do_shortcode( '[formidable id='.$contact[0]["formidable_id"].']' ); ?>
                      </div>
                    </div>

                    <div class="contact-image fivecol last">
                      <img src="<?php echo $contact[0]['image'];?>"/>
                    </div>

                  </div>
                </footer>

              </article> <!-- end article -->

              <?php endwhile;  endif; ?>

            </div> <!-- end #main -->

           </div> <!-- end #inner-content -->

      </div> <!-- end #content -->

<?php get_footer(); ?>
