<div class="inner-product-tabs desktop-tabs">
  
  <div id="tabs" class="tabs clearfix">
    <div class="tabs-inner wrap">
      <?php 
      $tabs = get_field('product_tabs');
      foreach($tabs as $tab) : 
        $content_image = wp_get_attachment_image_src( $tab['product_content'][0]['content_image'], 'medium' );
      ?>
        <div id="<?php echo to_slug($tab['product_title']); ?>"class="tab clearfix <?php if ($tab === reset($tabs)) echo 'tab-active';?>">
          <div class="content-image threecol">
            <img src="<?php echo $content_image[0]?>"/>
          </div>
          <div class="content-inner ninecol last">
            <h2 class="content-title"><?php echo $tab['product_content'][0]['content_header']; ?></h2>
            <p><?php echo $tab['product_content'][0]['content_text']; ?></p>
            <p><a class="push-button" href="<?php echo $tab['product_content'][0]['product_button_link'];?>"><?php echo $tab['product_content'][0]['product_button_text']; ?></a></p>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  </div>

  <div class="tabs-navigation">
    <ul class="tabs-nav wrap">
    <?php
    foreach ($tabs as $nav) : 
      $icon_normal   = wp_get_attachment_image_src( $nav['product_icons'][0]['icon_normal'], 'thumbnail' );
      $icon_hover    = wp_get_attachment_image_src( $nav['product_icons'][0]['icon_hover'], 'thumbnail' );
    ?>
      <li class="tab-link <?php if ($nav === reset($tabs)) echo 'tab-current';?>" data-link="<?php echo to_slug($nav['product_title']); ?>">
        <div class="icon-wrap">
          <img class="icon-hover" src="<?php echo $icon_hover[0]; ?>"/>
          <img class="icon-normal" src="<?php echo $icon_normal[0]; ?>"/>
        </div>
        <p class="tab-title"><?php echo $nav['product_title']; ?></p>
      </li>
    <?php endforeach; ?>
    </ul>
  </div>

</div><!-- / desktop-tabs -->