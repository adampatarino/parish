jQuery(document).ready(function($) {
    
    // Mobile Nav Toggle
    $('.nav-toggle').click(function() {
        $(this).parents('nav').toggleClass('active');
    });

    // Mobile Sub Nav Toggle
    $('#menu-main > li').has('ul').append('<div class="sub-nav-toggle"></div>');
    $('#menu-main > li > .sub-nav-toggle').click(function() {
        $(this).parents('li').toggleClass('sub-active');
    });

    // Mobile Product Tabs Toggle
    $('.product-header').click(function(){
        $(this).siblings('.product-content').toggleClass('tab-active');
    });

    // Desktop Tabs Setup
    var tabHeight = $('.tab.tab-active').height();
    $('.tabs-inner').height(tabHeight);

    // Desktop Change Tabs
    function changeTab(element) {
        var tabLink = '#' + element.data('link');
        $(tabLink).addClass('tab-active');
        $(tabLink).siblings('.tab-active').removeClass('tab-active');
        element.addClass('tab-current');
        element.siblings('.tab-current').removeClass('tab-current');
        $('.tabs-inner').height($(tabLink).height());
    }

    // Change Tab Every 8 Seconds
    var tabTimer = setInterval(function(){
        changeTab( $('.tab-current').next() );
    }, 8000);  
  

    // Change Tab on Hover of List Item
    $('.tab-link').hover(function() {
       changeTab( $(this) ); 
       clearInterval( tabTimer );
    });

    // Video Lightbox
    $('.video-lightbox').magnificPopup({type:'iframe'});

        

});




// IE8 ployfill for GetComputed Style (for Responsive Script below)
if (!window.getComputedStyle) {
    window.getComputedStyle = function(el, pseudo) {
        this.el = el;
        this.getPropertyValue = function(prop) {
            var re = /(\-([a-z]){1})/g;
            if (prop == 'float') prop = 'styleFloat';
            if (re.test(prop)) {
                prop = prop.replace(re, function () {
                    return arguments[2].toUpperCase();
                });
            }
            return el.currentStyle[prop] ? el.currentStyle[prop] : null;
        }
        return this;
    }
}

// as the page loads, call these scripts
jQuery(document).ready(function($) {
    
    /* getting viewport width */
    var responsive_viewport = $(window).width();
    
    /* if is below 481px */
    if (responsive_viewport < 768) {
        

    } /* end smallest screen */
    
    /* if is larger than 481px */
    if (responsive_viewport > 481) {
        
    } /* end larger than 481px */
    
    /* if is above or equal to 768px */
    if (responsive_viewport >= 768) {
    
        /* load gravatars */
        $('.comment img[data-gravatar]').each(function(){
            $(this).attr('src',$(this).attr('data-gravatar'));
        });
        
    }
    
    /* off the bat large screen actions */
    if (responsive_viewport > 1030) {
        
    }	
 
}); /* end of as page load scripts */


/*! A fix for the iOS orientationchange zoom bug.
 Script by @scottjehl, rebound by @wilto.
 MIT License.
*/
(function(w){
	// This fix addresses an iOS bug, so return early if the UA claims it's something else.
	if( !( /iPhone|iPad|iPod/.test( navigator.platform ) && navigator.userAgent.indexOf( "AppleWebKit" ) > -1 ) ){ return; }
    var doc = w.document;
    if( !doc.querySelector ){ return; }
    var meta = doc.querySelector( "meta[name=viewport]" ),
        initialContent = meta && meta.getAttribute( "content" ),
        disabledZoom = initialContent + ",maximum-scale=1",
        enabledZoom = initialContent + ",maximum-scale=10",
        enabled = true,
		x, y, z, aig;
    if( !meta ){ return; }
    function restoreZoom(){
        meta.setAttribute( "content", enabledZoom );
        enabled = true; }
    function disableZoom(){
        meta.setAttribute( "content", disabledZoom );
        enabled = false; }
    function checkTilt( e ){
		aig = e.accelerationIncludingGravity;
		x = Math.abs( aig.x );
		y = Math.abs( aig.y );
		z = Math.abs( aig.z );
		// If portrait orientation and in one of the danger zones
        if( !w.orientation && ( x > 7 || ( ( z > 6 && y < 8 || z < 8 && y > 6 ) && x > 5 ) ) ){
			if( enabled ){ disableZoom(); } }
		else if( !enabled ){ restoreZoom(); } }
	w.addEventListener( "orientationchange", restoreZoom, false );
	w.addEventListener( "devicemotion", checkTilt, false );
})( this );