<div class="content_block_2 <?php if (get_sub_field('layout_class')) {echo get_sub_field('layout_class');} ?> clearfix">

<div class="module content sixcol">
  <?php the_sub_field('column_1'); ?>
</div>  

<div class="module content sixcol last">
  <?php the_sub_field('column_2'); ?>
</div>  

</div>

