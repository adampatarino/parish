			<footer class="footer" role="contentinfo">

				<div id="inner-footer" class="wrap clearfix">
					<div class="footer__upper">
						<div class="footer__upper-left fourcol">
					
					<nav role="navigation">
							<?php bones_footer_links(); ?>
					</nav>
						</div>
						<div class="footer__upper-mid fourcol"><h6><strong>The Latest News from Parish</strong></h6><br/>
							<?php $the_query = new WP_Query( 'showposts=1' ); ?>
							<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
							<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
							<?php the_excerpt(__('(Read More)')); ?>
							<?php endwhile;?>
						</div>
						<div class="footer__upper-right fourcol last">
						<h5><?php the_field('company_name', 'options'); ?></h5>
						<div class="footer_info"><?php the_field('street_address', 'options'); ?></div>
						<div class="footer_info"><?php the_field('city_state_zip', 'options'); ?></div>
						<div class="footer_info">Phone: <?php the_field('phone', 'options'); ?></div>
						<div class="footer_info">Toll Free: <?php the_field('toll_free', 'options'); ?></div>
						<div class="footer_info">Fax: <?php the_field('fax', 'options'); ?></div>
						</div>	
					</div>
					
					
				</div> 
				<div class="footer__lower">
				<div class="wrap">
				<div class="footer__lower-left"><img src="<?php echo get_stylesheet_directory_uri().'/library/images/hive.png';?>"></div>				<div class="footer__lower-right"><p class="source-org copyright">Copyright 2013 Parish Manufacturing Inc. <a href="#">Privacy Policy</a></p></div></div>
					</div><!-- end #inner-footer -->

			</footer> <!-- end footer -->

		</div> <!-- end #container -->

		<!-- all js scripts are loaded in library/bones.php -->
		<?php wp_footer(); ?>

	</body>

</html> <!-- end page. what a ride! -->
