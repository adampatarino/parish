<?php get_header(); 

global $detect;?>

      <div id="content">
        
        <?php $slides = get_field('slider');
        if ( $slides ) :
          $slide_index = rand(0, count($slides)-1);
          if ( $detect->isMobile() ) {
            $slide_size = 'large';
          } else {
            $slide_size = 'full';
          }
          $slide_id = $slides[$slide_index]['slide_image'];
          $slide_image = wp_get_attachment_image_src( $slide_id, $slide_size );
        ?>
          
          <section id="slider" class="slider">
            <div class="inner-slider" style="background-image: url('<?php echo $slide_image[0];?>');">
              
              <div class="slider-content wrap clearfix">

              <?php if ( $slides[$slide_index]['title'] ) {?>
                <header class="slider-header">
                  <h2><?php echo $slides[$slide_index]['title']; ?></h2>
                </header>
              <?php } ?>
              
              <?php if ( $slides[$slide_index]['button_link'] ) {?>
              <footer class="slider-cta">
                <a class="button" href="<?php echo $slides[$slide_index]['button_link']; ?>"><?php echo $slides[$slide_index]['button_text']; ?></a>
              </footer>
              <?php } ?>

              </div>

            </div>
          </section><!-- End Slider -->

          <?php endif; ?>

          <section id="product-tabs" class="product-tabs">
            <?php // Serve Tabs Markup based on device
            if ( $detect->isMobile() ) {
              include_once('partials/mobile_tabs.php');
            } else {            
              include_once('partials/desktop_tabs.php');
            }
            ?>
          </section><!-- End Tabs -->

          <section id="marketing" class="marketing">
            <div class="inner-marketing wrap cleafix">
              
              <div class="marketing-message sixcol">
                <?php $marketing = get_field('marketing_message');
                if ( $marketing ) :?>
                  <h1 class="marketing-title"><?php echo $marketing[0]['title']; ?></h1>
                  <p class="magketing-content"><?php echo $marketing[0]['content']; ?></p>
                <?php endif; ?>
              </div>
              
              <div class="hot-news sixcol last">
                <h3 class="hot-news-title">Hot News</h3>
                <?php
                $args = array (
                  'post_type' => 'post',
                  'posts_per_page' => 2,
                  'orderby' => 'date',
                  'order' => 'DESC'
                );
                $the_query = new WP_Query( $args );
                if ( $the_query->have_posts() ) :
                  while ( $the_query->have_posts() ) :
                  $the_query->the_post(); ?>
                  
                  <article id="post-<?php the_ID(); ?>" <?php post_class( 'post clearfix' ); ?> role="article">
                    <header class="article-header">
                      <p class="post-date"><?php the_date('M d Y'); ?></p>
                      <h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
                    </header>
                    <section class="entry-content clearfix">
                      <?php the_excerpt(); ?>
                    </section>
                    <footer class="article-footer">
                      <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">Find out more</a>
                    </footer>
                  </article>

                <?php endwhile; endif; wp_reset_query();?>

                  <div class="more-posts"><a href="/blog/">See previous news <img src="<?php echo get_stylesheet_directory_uri().'/library/images/icons/previous-arrow.svg';?>"/></a></div>
              </div>

            </div>
          </section><!-- End Marketing -->

          
          <section id="video" class="home-video">
            <div class="inner-video wrap clearfix">
              
              <?php $video = get_field('product_video');             
              $video_icon = wp_get_attachment_image_src( $video[0]['product_video_icon'], 'medium' ); ?>
              <div class="video-content sevencol">
                <h2 class="video-title"><?php echo $video[0]['product_video_title']; ?></h2>
                <p class="video-sub-title"><?php echo $video[0]['product_video_sub_title']; ?></p>
                <a class="push-button video-lightbox" href="<?php echo $video[0]['product_video_link']; ?>">Watch the Video Now</a>
              </div>

              <div class="video-icon fivecol last">
                <a class="video-lightbox" href="<?php echo $video[0]['product_video_link']; ?>">
                  <img src="<?php echo $video_icon[0]; ?> "/>
                </a>
              </div>

            </div>
          </section>

          <section id="featured-products" class="featured-products">
            <div class="featured-products-inner wrap clearfix">
              <div class="products-pattern"></div>

              <?php $products_section = get_field('featured_products'); ?>
              <div class="products-content fivecol omega last">
                <h2><?php echo $products_section[0]['section_title'];?></h2>
                <p><?php echo $products_section[0]['section_content'];?></p>
              </div>
              
              <div class="products-images sevencol">
                <?php $products = $products_section[0]['products'];
                if ( $products ) : foreach ($products as $product) : 
                  $product_image = wp_get_attachment_image_src( $product['product_image'], 'medium' );?>
                  
                  <div class="product">
                    <a href="<?php echo $product['product_link'];?>">
                      <img src="<?php echo $product_image[0]?>"/>
                      <p><?php echo $product['product_name']; ?></p>
                    </a>
                  </div>

                <?php endforeach; endif; ?>
              </div>
            
            </div>
          </section>

          <section id="connect" class="connect">         
            <div class="connect-left fivecol"></div>
            

            <div class="connect-inner wrap clearfix">
              
              <div class="connect-top sevencol">
                <?php $connect = get_field('connect'); ?>
                <h2><?php echo $connect[0]['connect_title']; ?></h2>
                <p><?php echo $connect[0]['connect_content'];?></p>
                
                <?php $social_media = get_field('social_media', 'option');
                if ($social_media) : ?>
                  <div class="connect-social clearfix">
                  <h4 class="sixcol">Connect with us</h4>
                    <div class="social-media sixcol last">
                      <?php foreach ($social_media as $profile) : ?>
                        <div class="social-profile">
                          <a href="<?php echo $profile['social_url'];?>">
                            <img src="<?php echo $profile['social_icon'];?>"/>
                          </a>
                        </div>
                      <?php endforeach; ?>
                    </div>
                  </div>
                <?php endif; ?>
              </div>
              
              <div class="connect-bottom fivecol last">
                <h3><?php echo $connect[0]['contact_title']; ?></h3>
                <p><?php echo $connect[0]['contact_content'];?></p>
                <p><?php if ($connect[0]['contact_button_link']) {?>
                  <a class="button" href="<?php echo $connect[0]['contact_button_link'];?>"><?php echo $connect[0]['contact_button_text']; ?></a>
                <?php } ?>
              </div>
            
            </div>
          </section>


        
        
      </div> <!-- end #content -->

<?php get_footer(); ?>
