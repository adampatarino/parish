<!-- Section #product-tabs -->
  <div class="inner-product-tabs mobile-tabs">
    <h2 class="products-title">Bag in Box Solutions...</h2>
    <?php $products = get_field('product_tabs');
    if ( $products ) : ?>
      <ul class="products">
      
      <?php foreach ($products as $product ) : 
        // Get the images and correct size
        $icon = wp_get_attachment_image_src( $product['product_icons'][0]['icon_hover'], 'thumbnail' );
      ?>
        <li class="product">
          <div class="product-header clearfix">
            <img class="icon" src="<?php echo $icon[0];?>"/>
            <h4 class="product-title"><?php echo $product['product_title']; ?></h4>
          </div>
          <div class="product-content">
            <p><?php echo $product['product_content'][0]['content_text']; ?></p>
            <p><a class="push-button" href="<?php echo $product['product_content'][0]['product_button_link'];?>"><?php echo $product['product_content'][0]['product_button_text']; ?></a></p>
          </div>
        </li>  
      <?php endforeach; ?>

      </ul>
    <?php endif; ?>

  </div>
<!-- / Mobile Product Tabs -->