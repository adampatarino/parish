<?php
// ===================================================
// Load database info and local development parameters
// ===================================================
if ( file_exists( dirname( __FILE__ ) . '/local-config.php' ) ) {
	define( 'WP_LOCAL_DEV', true );
	include( dirname( __FILE__ ) . '/local-config.php' );
} else {
	define( 'WP_LOCAL_DEV', false );
	define( 'DB_NAME', 'azdevcom_pardb' );
	define( 'DB_USER', 'azdevcom_pardb' );
	define( 'DB_PASSWORD', '6Ssx7tfP9c' );
	define( 'DB_HOST', 'localhost' ); // Probably 'localhost'
}

// ================================================
// You almost certainly do not want to change these
// ================================================
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );

// ==============================================================
// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
// ==============================================================
define('AUTH_KEY',         'sbr8yxzy9ownyvqdbvh3zlrv9565kzrph8wvaalbjbuxgvohskthskgobbttu8yd');
define('SECURE_AUTH_KEY',  'cqko56qdxhjvzkz8hwugfbi5pc8v18rxyqkvxxgyjdvzna4ilrltqgk5ftlw1ben');
define('LOGGED_IN_KEY',    'bgs6tqllrlsaknsj0vwtse0n4yb2e9w9vzpa42nwryjgyflncdwbno19isqicr3u');
define('NONCE_KEY',        'zzgytodsppwnchtfzjrqw9j5rhrg60wareu4q5ifzte917egway976qexf2qg7zk');
define('AUTH_SALT',        '0mpipflrrdl85b7ly6vqqxdsapd7xyutyekgatud2gxnkaebdxniwxcnxx42urro');
define('SECURE_AUTH_SALT', 'huzydlbe8iabyur3u8uvayulilnzwqbb0jdox1hg9nhxae2i6vqxd13gx7ob0scu');
define('LOGGED_IN_SALT',   '7b7iy7h5dey6y8ibqdbxsr9ofeypt6yjebskdvzxiqiklur8ssahdht6lz1letwh');
define('NONCE_SALT',       'ydb1mulewulnxey2fphju12e6pmk2iq9w4fgm8f07an7rureqfogc5lyizsub2jt');


// ==============================================================
// Table prefix
// Change this if you have multiple installs in the same database
// ==============================================================
$table_prefix  = 'wp_';

// ================================
// Language
// Leave blank for American English
// ================================
define( 'WPLANG', '' );

// ===========
// Hide errors
// ===========
ini_set( 'display_errors', 0 );
define( 'WP_DEBUG_DISPLAY', false );

// =================================================================
// Debug mode
// Debugging? Enable these. Can also enable them in local-config.php
// =================================================================
// define( 'SAVEQUERIES', true );
// define( 'WP_DEBUG', true );

// ======================================
// Load a Memcached config if we have one
// ======================================
if ( file_exists( dirname( __FILE__ ) . '/memcached.php' ) )
	$memcached_servers = include( dirname( __FILE__ ) . '/memcached.php' );

// ===========================================================================================
// This can be used to programatically set the stage when deploying (e.g. production, staging)
// ===========================================================================================
define( 'WP_STAGE', '%%WP_STAGE%%' );
define( 'STAGING_DOMAIN', '%%WP_STAGING_DOMAIN%%' ); // Does magic in WP Stack to handle staging domain rewriting

// ===================
// Bootstrap WordPress
// ===================
if ( !defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/wp/' );
require_once( ABSPATH . 'wp-settings.php' );
