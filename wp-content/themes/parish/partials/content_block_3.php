<div class="content_block_3 <?php if (get_sub_field('layout_class')) {echo get_sub_field('layout_class');} ?> clearfix">

<div class="module content fourcol">
  <?php the_sub_field('column_1'); ?>
</div>  

<div class="module content fourcol">
  <?php the_sub_field('column_2'); ?>
</div> 

<div class="module content fourcol last">
  <?php the_sub_field('column_3'); ?>
</div>  

</div>