<?php
/* Template Name: Modules */
get_header(); ?>

      <div id="content">

        <div id="inner-content" class="clearfix">

            <div id="main" class="clearfix" role="main">

              <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

              <article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

                <header class="page-header">
				 <div class="header-image"><img src="<?php the_field('header_image') ?>"></div>	
                 <div class="wrap"><div class="header_title"> <h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1> 
                 <h3> <?php the_field('sub-header'); ?></h3></div></div>

                </header> <!-- end article header -->

                <section class="entry-content wrap" itemprop="articleBody">
                  <?php 
                  while(has_sub_field("layout_modules")):
                      $format = get_row_layout();
                      get_template_part( 'partials/'.$format);
                  endwhile; ?>
                </section> <!-- end article section -->

              </article> <!-- end article -->

              <?php endwhile;  endif; ?>

            </div> <!-- end #main -->

           </div> <!-- end #inner-content -->

      </div> <!-- end #content -->

<?php get_footer(); ?>
