<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<!-- Google Chrome Frame for IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title><?php wp_title(''); ?></title>

		<!-- mobile meta (hooray!) -->
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<!-- icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) -->
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<!-- or, set /favicon.ico for IE10 win -->
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->

		<!-- drop Google Analytics Here -->
		<!-- end analytics -->

	</head>

	<body <?php body_class(); ?>>

		<div id="container">

			<header class="header" role="banner">

				<div id="inner-header" class="inner-header wrap clearfix">
					
					<div class="header-top clearfix">
						<div id="logo" class="threecol">
							<a href="<?php echo home_url(); ?>" rel="nofollow">
								<img class="logo" src="<?php the_field('logo', 'option') ?>"/>
							</a>
						</div>
						
						<?php $numbers = get_field('phone_numbers', 'option'); ?>
							<div class="phone-numbers fivecol">
								
								<img class="phone-icon" src="<?php echo get_stylesheet_directory_uri().'/library/images/icons/phone.svg';?>"/>
								<?php if($numbers[0]['phone']) {?>
									<div class="phn phone-standard">
										<strong>Phone:</strong> 
										<em><?php echo $numbers[0]['phone']; ?></em>
									</div>
								<?php } ?>

								<?php if($numbers[0]['toll_free']) {?>
									<div class="phn phone-toll-free">
										<strong>Toll Free:</strong> 
										<em><?php echo $numbers[0]['toll_free']; ?></em>
									</div>
								<?php } ?>

							</div>						

						<div class="header-contact fourcol omega">
							<div class="contact-inner clearfix">
								
								<div class="contact fourcol">
									<a href="/contact/"><img class="contact-icon" src="<?php echo get_stylesheet_directory_uri().'/library/images/icons/contact.svg';?>"/></a>
									<span class="contact-label">Contact Us</span>
								</div>
								
								<?php $social_media = get_field('social_media', 'option');
								if ($social_media) : ?>
									<div class="social-media eightcol last">
										<?php foreach ($social_media as $profile) : ?>
											<div class="social-profile">
												<a href="<?php echo $profile['social_url'];?>">
													<img src="<?php echo $profile['social_icon'];?>"/>
												</a>
					        		</div>
										<?php endforeach; ?>
										<span class="contact-label">Connect With Us</span>
									</div>
								<?php endif; ?>

							</div>
						</div>
					</div>

					<nav role="navigation" class="main-nav">
						
						<img class="nav-toggle" src="<?php echo get_stylesheet_directory_uri().'/library/images/icons/nav-toggle.svg'?>"/>
						
						<?php bones_main_nav(); ?>
						
						<div class="mobile-contact">
							
							<a class="contact-link" href="/contact/"><img class="contact-icon" src="<?php echo get_stylesheet_directory_uri().'/library/images/icons/contact-blue.svg';?>"/> <span>Contact Us</span></a>
							
							<div class="mobile-phone-numbers clearfix">
								
								<img class="phone-icon" src="<?php echo get_stylesheet_directory_uri().'/library/images/icons/phone.svg';?>"/>
								
								<div class="numbers-wrap">
									<?php if($numbers[0]['phone']) {?>
										<div class="phn phone-standard">
											<strong>Phone:</strong> 
											<em><?php echo $numbers[0]['phone']; ?></em>
										</div>
									<?php } ?>

									<?php if($numbers[0]['toll_free']) {?>
										<div class="phn phone-toll-free">
											<strong>Toll Free:</strong> 
											<em><?php echo $numbers[0]['toll_free']; ?></em>
										</div>
									<?php } ?>
								</div>

							</div>

						</div>
						
					</nav>

				</div> <!-- end #inner-header -->

			</header> <!-- end header -->
